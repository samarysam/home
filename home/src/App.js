import React, { Component } from 'react';

//stylish boiss
import './App.css';

//rebase
import Rebase from 're-base';

//firebase stuff
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/database';

// plain old components
import Member from './Component/Member'

const config = {
  apiKey: "AIzaSyC2CtFTamSm_frnxNAsG7PR_PXXcHSo_1g",
  authDomain: "lambdahome-ca96b.firebaseapp.com",
  databaseURL: "https://lambdahome-ca96b.firebaseio.com",
  projectId: "lambdahome-ca96b",
  storageBucket: "lambdahome-ca96b.appspot.com",
  messagingSenderId: "930250855005"
};

var app = firebase.initializeApp(config);
var db = firebase.database(app);
const base= Rebase.createClass(db);


const InitialState = {
  isSignedIn: false,
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {...InitialState}
  }

  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false
    }
  };

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
        (user) => this.setState({isSignedIn: !!user})
    );
  }
  

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  signout= () => {
    firebase.auth().signOut();
  }

  render() {
    if (!this.state.isSignedIn) {
      return (
        <div className="Mycontainer">
          <div className="logoContainer">
            <img className = "lambdaLogo" alt="" src={require('./logo.png')} />
            {/* <p>    Home </p> */}
          </div>
          <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()}/>
        </div>
      );
    }
    return (
      <div>
        <Member signout={this.signout} base = {base} uid={firebase.auth().currentUser.uid}/>
        {console.log(firebase.auth().currentUser.displayName)}
        {console.log(firebase.auth().currentUser.email)}
        {console.log(firebase.auth().currentUser.uid)}
        {console.log('props',this.props)}
        {console.log('state',this.state)}
      </div>
    );
  }
}

export default App;

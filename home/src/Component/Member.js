import React, { Component } from 'react';
import { Table, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap';
import './Member.css'


const InitialState = {
    user: {}
  }
export default class Member extends Component {
    constructor(props){
        super(props);
        this.state = {...InitialState}
      }

    componentDidMount() {
        this.ref = this.props.base.syncState(`users/${this.props.uid}`, {
            context: this,
            state: 'user'
        });

        console.log(`users/${this.props.uid}`)
        
    }

    addItem(newItem){
        this.setState({
          items: this.state.items.concat([newItem]) //updates Firebase and the local state
        });
      }

    componentWillUnmount() {
        this.props.base.removeBinding(this.ref);
    }

  render() {
    return (
      <div className="memberContainer">
      {console.log('memberva',this.state)}
        <div className="MyNav">
        <div>
          <img  alt="" src={require('../logo.png')} />
        </div>
        <h1>Student Directory</h1>
        <h2 onClick={() => this.props.signout()}>Sign Out</h2>
        </div>
        <div className="bodyContainer">
          <div className="cardContainer">
          <Card style={{width: 20 + 'rem'}}>
                <CardImg top width="300px" height = "300px" src="https://robohash.org/saamar"  alt="Card image cap" />
                <CardBody>
                <CardTitle> Samarvir Singh</CardTitle>
                <CardSubtitle>bio -</CardSubtitle>
                <CardText>Wubba lubba dub dub mfs...</CardText>
                </CardBody>
            </Card>
          </div>
          <div className="tableContainer">
          <Table striped>
        <tbody>
          <tr>
            <td>Name</td>
            <td>Samarvir Singh</td>
          </tr>
          <tr>
            <td>Class</td>
            <td>cs10</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>samarvir1996@gmail.com</td>
          </tr>
          <tr>
            <td>Phone</td>
            <td>+91 9953167795</td>
          </tr>
          <tr>
            <td>Github</td>
            <td>https://github.com/samarv</td>
          </tr>
          <tr>
            <td>Linked</td>
            <td>https://linkedin.com/in/samar-vir/</td>
          </tr>
        </tbody>
      </Table>
        </div>
        </div>
      </div>
    )
  }
};
